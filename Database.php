<?php

namespace FpDbTest;

use Exception;
use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function buildQuery(string $query, array $args = []): string
    {
        $regex = '/\?d|\?f|\?a|\?#|\?/';

        $query = preg_replace_callback($regex, function ($matches) use (&$args) {
            $match = $matches[0];
            if ($match === '?') {
                $value = array_shift($args);
                $value = $this->escapeValue($value);
            } elseif ($match === '?d') {
                $value = array_shift($args);
                $value = $this->escapeValue($value);
            } elseif ($match === '?f') {
                $value = array_shift($args);
                $value = $this->escapeValue($value);
            } elseif ($match === '?a') {
                $value = array_shift($args);
                $innerArgs = [];
                foreach ($value as $key => $v) {
                    if (is_int($key)) {
                        $innerArgs[] = $this->escapeValue($v);
                    } else {
                        $innerArgs[] = "{$this->escapeIdentifier($key)} = {$this->escapeValue($v)}";
                    }
                }

                $value = implode(', ', $innerArgs);
            } elseif ($match === '?#') {
                $value = array_shift($args);
                if (is_array($value)) {
                    $value = implode(', ', array_map(function ($item) {
                        return $this->escapeIdentifier($item);
                    }, $value));
                } else {
                    $value = $this->escapeIdentifier($value);
                }
            } else {
                throw new \InvalidArgumentException('Unsupported template: ' . $match);
            }

            return $value;
        }, $query);

        $pattern = '/\{[^{]*' . $this->skip() . '[^}]*\}/';
        $query = preg_replace($pattern, '', $query);
        $query = str_replace(['{', '}'], '', $query);

        return $query;
    }

    public function skip(): string
    {
        return 'SKIP';
    }

    private function escapeValue(string|int|float|bool|null $value): string|int|float
    {
        if ($value === null) {
            return 'NULL';
        }

        if (is_int($value) || is_float($value)) {
            return $value;
        }

        if (is_bool($value)) {
            return $value ? 1 : 0;
        }

        if (is_string($value)) {
            return "'{$this->mysqli->real_escape_string($value)}'";
        }

        throw new \InvalidArgumentException();
    }

    private function escapeIdentifier(string $identifier): string
    {
        if (is_string($identifier)) {
            return "`{$this->mysqli->real_escape_string($identifier)}`";
        }

        throw new \InvalidArgumentException();
    }
}